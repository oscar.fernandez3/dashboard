# imagen base
FROM ubuntu
# ejecutar un comando dentro del contenedor temporal
RUN apt update && apt install apache2 -y
# Copia ficheros y directorios desde fuera hacia adentro
COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf
# Abre puertos del contenedor
EXPOSE 80 443
# Define el comando o programa que se ejecuta
# cuando se inicia el contenedor
# ['comando', '-opcion', 'argumentos']
CMD ["apache2ctl", "-D", "FOREGROUND"]
