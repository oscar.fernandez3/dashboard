https://gitlab.com/oscar.fernandez3/dashboard
TP FINAL
Nombre y Apellido: OSCAR FERNANDEZ
Materia: INFRAESTRUCTURA EN LA NUBE 
Profesor: SERGIO PERNAS

Mi App
Dockerfile para la creacion y despliegue de la aplicacion Mi App.
Creación de la imagen
Clonar repositorio
$ git clone https://gitlab.com/oscar.fernandez3/dashboard 

Una vez clonado el repo acceder al directorio.
$ cd dashboard
Creación de imagen
$ docker build -t mi-app-img .
Nota: Antes de construir la imagen posicionarse en la rama correspondiente (main, develop, testing).
Despliegue del contenedor
Mediante comandos Docker.
$ docker run -d -p 8000:80 --name mi-app mi-app-img
Mediante compose.
$ docker-compose up -d
Proxy reverso
Añadir esta configuración al servidor proxy reverso.
server {
    listen 80;
    server_name mi-app.com;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://ip_docker_server:8000;
    }
}
Nota: Archivo de vitualhost de nginx 'mi-app.conf' .




DOCKERFILE

FROM ubuntu

RUN apt update && apt install apache2 git -y

COPY index.html /var/www/html/index.html

EXPOSE 80

CMD ["apachectl", "-D", "FOREGROUND"]


docker-compose.yaml115 B
Blame
Edit
1
2
3
4
5
6
7
8
9
10
version: '3.8'
services:
  mi-app:
    image: mi-app-img
    container_name: mi-app
    ports:
      - "8000:80"
version: '3.8'

services:
  mi-app:
    image: mi-app-img
    container_name: mi-app
    ports:
      - "8000:80"

INDEX.HTML
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi App</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #e8f5e9;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .container {
            text-align: center;
            background-color: #ffffff;
            padding: 2rem;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            color: #388e3c;
        }
        p {
            color: #66bb6a;
        }
        .button {
            display: inline-block;
            padding: 10px 20px;
            margin-top: 20px;
            color: #ffffff;
            background-color: #388e3c;
            border-radius: 5px;
            text-decoration: none;
            transition: background-color 0.3s ease;
        }
        .button:hover {
            background-color: #2e7d32;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Bienvenido a Mi App</h1>
        <p>Esta es una aplicación de ejemplo creada con Docker y Apache2.</p>
        <a href="#" class="button">Empezar</a>
    </div>
</body>
</html>




MI-APP.CONF


server {
    listen 80;
    server_name mi-app.com;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://ip_docker_server:8000;
    }
}












